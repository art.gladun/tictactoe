import React from "react";
import { Route, Routes, useNavigate } from "react-router-dom";

import { PageLayout } from "components/Page";

import { TicTacToePage } from "views/TicTacToe";

import { PATHS } from "./paths";
import { useEffect } from "react";

const Redirect = () => {
  const navigate = useNavigate();

  useEffect(() => {
    navigate(PATHS.TICTACTOE.ROOT.toPath());
  }, [navigate]);

  return null;
};

export const Pages = () => {
  return (
    <PageLayout>
      <Routes>
        <Route path={PATHS.TICTACTOE.ROOT.PATH} element={<TicTacToePage />} />
        <Route path="*" element={<Redirect />} />
      </Routes>
    </PageLayout>
  );
};
