import { useAuthenticator } from "@aws-amplify/ui-react";

import { PowerOffIcon } from "components/Icons";
import { Button } from "components/Button";

export const BottomBar = () => {
  const { user, signOut } = useAuthenticator();

  return (
    <div className="flex gap-4 items-end fixed right-0 bottom-0 p-4">
      <span className="text-sm">{user.userId}</span>
      <Button onClick={signOut} variant="ghost" size="xl">
        <PowerOffIcon />
      </Button>
    </div>
  );
};
