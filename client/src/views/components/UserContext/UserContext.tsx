import { createContext, useState, useEffect } from "react";
import { useAuthenticator } from "@aws-amplify/ui-react";
import { AuthUser } from "aws-amplify/auth";
import { getAuthToken } from "data/session";

interface UserContextState {
  user: AuthUser;
  authToken: string;
}

export const UserContext = createContext<UserContextState>({} as any); // the any is fine here, this is inside the Authenticator provider

export const UserContextProvider: React.FC<React.PropsWithChildren> = ({
  children,
}) => {
  const value = useAuthenticator((context) => [context.user]);
  const [authToken, setAuthToken] = useState<string | undefined>();

  useEffect(() => {
    getAuthToken().then(setAuthToken);
  }, []);

  if (!authToken) {
    return null;
  }

  return (
    <UserContext.Provider value={{ ...value, authToken }}>
      {children}
    </UserContext.Provider>
  );
};
