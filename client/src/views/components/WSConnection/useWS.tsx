import { PropsWithChildren, createContext, useContext, useEffect } from "react";
import useWebSocket, { ReadyState } from "react-use-websocket";
import { WebSocketHook } from "react-use-websocket/dist/lib/types";

import { UserContext } from "../UserContext";

const connectionStatus: Record<number, string> = {
  [ReadyState.CONNECTING]: "Connecting",
  [ReadyState.OPEN]: "Open",
  [ReadyState.CLOSING]: "Closing",
  [ReadyState.CLOSED]: "Closed",
  [ReadyState.UNINSTANTIATED]: "Uninstantiated",
};

const WSConnectionContext = createContext<WebSocketHook>(null as any);

export const WSConnectionContextProvider: React.FC<PropsWithChildren> = ({
  children,
}) => {
  const { authToken } = useContext(UserContext);

  const ws = useWebSocket(process.env.REACT_APP_WS_ENDPOINT!, {
    protocols: authToken,
    share: true,
    shouldReconnect: () => true,
  });

  useEffect(() => {
    console.log(
      new Date().toISOString(),
      " Connection: ",
      connectionStatus[ws.readyState],
    );
  }, [ws.readyState, authToken]);

  return (
    <WSConnectionContext.Provider value={ws}>
      {children}
    </WSConnectionContext.Provider>
  );
};

export const useWS = () => {
  return useContext(WSConnectionContext);
};
