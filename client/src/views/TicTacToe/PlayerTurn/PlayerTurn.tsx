import classNames from "classnames";

interface Props {
  yourTurn: boolean;
}

export const PlayerTurn: React.FC<Props> = ({ yourTurn }) => {
  const styles = classNames(
    "text-center text-3xl font-bold",
    yourTurn && "text-red-500",
    !yourTurn && "text-gray-400",
  );
  return (
    <div className={styles}>{yourTurn ? "Your tourn" : "Opponent turn"}</div>
  );
};
