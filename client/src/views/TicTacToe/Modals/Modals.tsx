import classNames from "classnames";
import { Button } from "components/Button";
import { Modal, ModalProps, ModalContent } from "components/Modal";

import { useLocale } from "locales";

import { GamePlayer, OverGame } from "data/tictactoe";

const modalContentStyles = classNames("text-center flex flex-col gap-6 py-16");
const messagStyles = classNames("text-6xl font-bold");

interface InitializedModalProps extends ModalProps {
  onClick: () => void;
}

export const InitializedModal: React.FC<InitializedModalProps> = ({
  onClick,
}) => {
  const { t } = useLocale();

  return (
    <Modal>
      <ModalContent className={modalContentStyles}>
        <div className={messagStyles}>{t("FIND_OPPONENT")}</div>
        <div>
          <Button onClick={onClick} size="xxxl">
            {t("PLAY_BUTTON_LABEL")}
          </Button>
        </div>
      </ModalContent>
    </Modal>
  );
};

interface DisconnectedModalProps extends ModalProps {
  onClick: () => void;
}

export const DisconnectedModal: React.FC<DisconnectedModalProps> = ({
  onClick,
}) => {
  const { t } = useLocale();

  return (
    <Modal>
      <ModalContent className={modalContentStyles}>
        <div className={messagStyles}>{t("YOUR_OPPONENT_LEAVE")}</div>
        <div>
          <Button onClick={onClick} size="xxxl">
            {t("PLAY_BUTTON_LABEL")}
          </Button>
        </div>
      </ModalContent>
    </Modal>
  );
};

interface WaitingForOpponentModalProps extends ModalProps {}

export const WaitingForOpponentModal: React.FC<
  WaitingForOpponentModalProps
> = () => {
  const { t } = useLocale();

  return (
    <Modal>
      <ModalContent className={modalContentStyles}>
        <div className={messagStyles}>{t("WAITING_FOR_OPPONENT")}</div>
      </ModalContent>
    </Modal>
  );
};

interface OverModalProps extends ModalProps {
  onClick: () => void;
  gameState: OverGame;
  whoAmI: GamePlayer;
}

export const OverModal: React.FC<OverModalProps> = ({
  onClick,
  gameState,
  whoAmI,
}) => {
  const { t } = useLocale();

  const draw = "draw" === gameState.winner;

  return (
    <Modal>
      <ModalContent className={modalContentStyles}>
        <div className={messagStyles}>
          {draw && t("GAME_OVER_MESSAGE_DRAW")}
          {!draw && whoAmI !== gameState.winner && t("GAME_OVER_MESSAGE_LOSS")}
          {!draw && whoAmI === gameState.winner && t("GAME_OVER_MESSAGE_WIN")}
        </div>
        <div>
          <Button onClick={onClick} size="xxxl">
            {t("PLAY_BUTTON_LABEL")}
          </Button>
        </div>
      </ModalContent>
    </Modal>
  );
};
