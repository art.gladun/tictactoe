import classNames from "classnames";

import { BOARD_SIZE, Mark } from "data/tictactoe";

import { BoardCell } from "./BoardCell";

const boardStyles = classNames("grid", "grid-cols-3", "grid-rows-3", "gap-2");

interface BoardProps {
  board: Array<Mark>;
  onShot: (position: number) => void;
  canPlay: boolean;
  playerMark: Mark;
}

export const Board: React.FC<BoardProps> = ({
  board = [],
  onShot,
  canPlay,
  playerMark,
}) => {
  const handleClick = (event: React.MouseEvent<HTMLTableElement>) => {
    const position = (event.target as HTMLTableElement).dataset.position;

    if (typeof position === "string") {
      onShot(Number(position));
    }
  };

  const cells = Array.from(
    { length: BOARD_SIZE * BOARD_SIZE },
    (_, position) => {
      return (
        <BoardCell
          key={position}
          data-cell={position}
          position={position}
          mark={board[position]}
          active={canPlay}
          playerMark={playerMark}
        />
      );
    },
  );

  return (
    <div className={boardStyles} onClick={handleClick}>
      {cells}
    </div>
  );
};
