import classNames from "classnames";
import { Mark } from "data/tictactoe";

import React from "react";

interface Props {
  mark: Mark;
  position: number;
  active: boolean;
  playerMark: Mark;
}

export const BoardCell: React.FC<Props> = React.memo(
  ({ mark, position, active, playerMark }) => {
    const cellStyles = classNames(
      "flex items-center justify-center bg-gray-100 rounded aspect-square",
      !mark && active ? "cursor-pointer hover:bg-gray-200" : "",
      active &&
        !mark &&
        playerMark === "X" &&
        "hover:before:content-['X'] hover:before:text-6xl hover:before:font-bold hover:before:text-gray-700",
      active &&
        !mark &&
        playerMark === "O" &&
        "hover:before:content-['O'] hover:before:text-6xl hover:before:font-bold hover:before:text-gray-400",
      mark === "X" &&
        "before:content-['X'] before:text-6xl before:font-bold before:text-gray-700",
      mark === "O" &&
        "before:content-['O'] before:text-6xl before:font-bold before:text-gray-400",
    );

    return <div className={cellStyles} data-position={position} />;
  },
);
