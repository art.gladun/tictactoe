import { useCallback, useContext, useEffect, useState } from "react";

import {
  Mark,
  getResult,
  GameState,
  OverGame,
  NotStartedGame,
  DisconnectedGame,
  GamePlayer,
  InProgressGame,
  playerMark,
  WaitingForServerGame,
  GameAction,
} from "data/tictactoe";
import { WS } from "data/ws";

import { UserContext } from "views/components/UserContext";
import { useWS } from "views/components/WSConnection";

const getNotStartedState = (): NotStartedGame => {
  return {
    board: [],
    progress: "NOT_STARTED",
  };
};

export const useGame = () => {
  const { user } = useContext(UserContext);
  const ws = useWS();
  const [gameState, setGameState] = useState<GameState>(getNotStartedState());

  const sendWS = useCallback(
    (gameState: GameState, action: GameAction = "game-inprogress") => {
      ws.sendJsonMessage({ action, data: gameState });
    },
    [ws],
  );

  useEffect(() => {
    if (ws.lastJsonMessage) {
      const { data } = ws.lastJsonMessage as WS<
        InProgressGame | OverGame | DisconnectedGame
      >;

      console.log("ws:", data);

      if (!(data && data.id)) {
        return;
      }

      setGameState(data);
    }
  }, [sendWS, ws.lastJsonMessage]);

  const startNewGame = async () => {
    const newState: WaitingForServerGame = {
      board: [],
      progress: "WAITING_FOR_SERVER",
    };

    setGameState(newState);
    sendWS(newState, "game-init");
  };

  const handleShot = (position: number) => {
    if (gameInProgress(gameState) && canPlay() && !gameState.board[position]) {
      const player = whoAmI(gameState);
      const board = [...gameState.board];
      board[position] = playerMark[player];

      const newGameState = {
        ...gameState,
        board,
      };

      const winner = getResult(newGameState);

      if (winner) {
        const overGameState: OverGame = {
          ...newGameState,
          progress: "OVER",
          winner,
        };

        setGameState(overGameState);
        sendWS(overGameState);
        return;
      }

      newGameState.progress = getNextPlayer(gameState);
      setGameState(newGameState);
      sendWS(newGameState);
    }
  };

  const getNextPlayer = (gameState: GameState) => {
    if (gameState.progress === "PLAYER_1_TURN") {
      return "PLAYER_2_TURN";
    }

    if (gameState.progress === "PLAYER_2_TURN") {
      return "PLAYER_1_TURN";
    }

    throw new Error("Not expected status");
  };

  const whoAmI = (gameState: InProgressGame | OverGame): GamePlayer => {
    return gameState.player1 === user.userId ? "player1" : "player2";
  };

  const canPlay = () => {
    if (!gameInProgress(gameState)) {
      return false;
    }

    const iAm = whoAmI(gameState);
    return (
      (gameState.progress === "PLAYER_1_TURN" && iAm === "player1") ||
      (gameState.progress === "PLAYER_2_TURN" && iAm === "player2")
    );
  };

  const gameInProgress = (
    gameState: GameState,
  ): gameState is InProgressGame => {
    return ["PLAYER_1_TURN", "PLAYER_2_TURN"].includes(gameState.progress);
  };

  const getPlayerMark = (): Mark => {
    if (!gameInProgress(gameState)) {
      return "";
    }

    return playerMark[whoAmI(gameState)];
  };

  const actions = {
    startNewGame,
    handleShot,
  };

  const logic = {
    canPlay: canPlay(),
    playerMark: getPlayerMark(),
    whoAmI,
  };

  return {
    gameState,
    actions,
    logic,
  };
};
