import { PageContainer, PageContent } from "components/Page";

import { useGame } from "./useGame";

import { Board } from "./Board";
import {
  InitializedModal,
  WaitingForOpponentModal,
  DisconnectedModal,
  OverModal,
} from "./Modals";

import { PlayerTurn } from "./PlayerTurn";

export const TicTacToePage = () => {
  const { gameState, actions, logic } = useGame();

  return (
    <PageContent>
      <PageContainer>
        <div className="max-w-md w-full mx-auto flex flex-col gap-4">
          <Board
            board={gameState.board}
            onShot={actions.handleShot}
            canPlay={logic.canPlay}
            playerMark={logic.playerMark}
          />
          {(gameState.progress === "PLAYER_1_TURN" ||
            gameState.progress === "PLAYER_2_TURN") && (
            <PlayerTurn yourTurn={logic.canPlay} />
          )}
          {gameState.progress === "NOT_STARTED" && (
            <InitializedModal onClick={actions.startNewGame} />
          )}
          {(gameState.progress === "WAITING_FOR_OPPONENT" ||
            gameState.progress === "WAITING_FOR_SERVER") && (
            <WaitingForOpponentModal />
          )}
          {gameState.progress === "DISCONNECTED" && (
            <DisconnectedModal onClick={actions.startNewGame} />
          )}
          {gameState.progress === "OVER" && (
            <OverModal
              gameState={gameState}
              whoAmI={logic.whoAmI(gameState)}
              onClick={actions.startNewGame}
            />
          )}
        </div>
      </PageContainer>
    </PageContent>
  );
};
