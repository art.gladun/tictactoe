import { composeUrl, PathParams } from "utils/url";

const ROOT_PATH = "/tictactoe";
const DETAIL_PATH = `${ROOT_PATH}/:gameId`;

export const ROOT = {
  PATH: ROOT_PATH,
  toPath: () => ROOT_PATH,
};

interface DetailPathParams extends PathParams {
  gameId: string;
}

export const DETAIL = {
  PATH: DETAIL_PATH,
  toPath: (pathParams: DetailPathParams) =>
    composeUrl(DETAIL_PATH, { pathParams }),
};
