import { BrowserRouter as Router } from "react-router-dom";
import { Authenticator } from "@aws-amplify/ui-react";

import { UserContextProvider } from "views/components/UserContext";
import { WSConnectionContextProvider } from "views/components/WSConnection";
import { BottomBar } from "views/components/BottomBar";

import { Pages } from "./views/Pages";

import "./App.css";
import "@aws-amplify/ui-react/styles.css";

const username = {
  label: "Email",
  placeholder: "Enter your email",
};

const formFields = {
  signIn: {
    username,
  },
  signUp: {
    username,
  },
  forgotPassword: {
    username,
  },
};

function App() {
  return (
    <Authenticator variation="modal" formFields={formFields}>
      <UserContextProvider>
        <WSConnectionContextProvider>
          <Router>
            <Pages />
          </Router>
          <BottomBar />
        </WSConnectionContextProvider>
      </UserContextProvider>
    </Authenticator>
  );
}

export default App;
