import { useState } from "react";

import tictactoeEn from "./en/tictactoe.json";

const resources = {
  en: {
    tictactoe: tictactoeEn,
  },
};

type LocaleResources = typeof resources;
type Locale = keyof LocaleResources;
type LocaleScope = keyof LocaleResources["en"];
type LangNameSpace = keyof LocaleResources["en"]["tictactoe"];

export const useLocale = (scope: LocaleScope = "tictactoe") => {
  const [locale, setLocale] = useState<Locale>("en");

  const t = (key: LangNameSpace) => {
    return resources[locale][scope][key];
  };

  return {
    t,
    setLocale,
  };
};
