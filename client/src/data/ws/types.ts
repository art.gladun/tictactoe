export interface WS<T> {
  action: string;
  data: T;
}
