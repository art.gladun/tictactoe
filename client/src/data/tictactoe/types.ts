export type GameProgress =
  | "NOT_STARTED"
  | "WAITING_FOR_OPPONENT"
  | "PLAYER_1_TURN"
  | "PLAYER_2_TURN"
  | "OVER";
export type GamePlayer = "player1" | "player2";
export type GameResult = GamePlayer | "draw";
export type XorO = "X" | "O";
export type Mark = XorO | "";

export type GameAction = "game-init" | "game-inprogress";

interface BaseGame {
  board: Array<XorO | "">;
}

interface BaseServerGame extends BaseGame {
  id: string;
  player1: string;
  connections: string[];
}

export interface NotStartedGame extends BaseGame {
  progress: "NOT_STARTED";
}

export interface WaitingForServerGame extends BaseGame {
  progress: "WAITING_FOR_SERVER";
}

export interface WaitingForOponentGame extends BaseServerGame {
  progress: "WAITING_FOR_OPPONENT";
}

export interface InProgressGame extends BaseServerGame {
  player2: string;
  progress: "PLAYER_1_TURN" | "PLAYER_2_TURN";
}

export interface DisconnectedGame extends BaseServerGame {
  player2: string;
  progress: "DISCONNECTED";
}

export interface OverGame extends BaseServerGame {
  player2: string;
  progress: "OVER";
  winner: GameResult;
}

export type GameState =
  | NotStartedGame
  | WaitingForServerGame
  | WaitingForOponentGame
  | InProgressGame
  | OverGame
  | DisconnectedGame;

export type ServerGameState = WaitingForOponentGame | InProgressGame | OverGame;
