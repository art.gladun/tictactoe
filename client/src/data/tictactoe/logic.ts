import { InProgressGame, GameResult, GamePlayer, XorO } from "./types";

export const BOARD_SIZE = 3;

export const playerMark: Record<GamePlayer, XorO> = {
  player1: "X",
  player2: "O",
};

export const getResult = (gameState: InProgressGame): GameResult | null => {
  const winningOptions = [
    // horizontally
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],

    // vertically
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],

    // diagonally
    [0, 4, 8],
    [2, 4, 6],
  ];

  const winningOption = winningOptions.find(
    ([i1, i2, i3]) =>
      ["X", "O"].includes(gameState.board[i1]) &&
      gameState.board[i1] === gameState.board[i2] &&
      gameState.board[i2] === gameState.board[i3],
  );

  if (winningOption) {
    const winningMark = gameState.board[winningOption[0]];
    return playerMark.player1 === winningMark ? "player1" : "player2";
  }

  if (gameState.board.filter(Boolean).length === BOARD_SIZE * BOARD_SIZE) {
    return "draw";
  }

  return null;
};
