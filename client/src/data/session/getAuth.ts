import { fetchAuthSession } from "aws-amplify/auth";

export const getAuthToken = async () => {
  const { idToken } = (await fetchAuthSession())?.tokens ?? {};
  return idToken?.toString();
};

interface AuthHeaders {
  Authorization: string;
}

export const getAuthHeaders = async (): Promise<AuthHeaders> => {
  const accessToken = await getAuthToken();
  return { Authorization: `${accessToken}` };
};
