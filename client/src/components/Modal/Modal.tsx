import classNames from "classnames";
import { PropsWithChildren } from "react";

const DialogBackdrop: React.FC<PropsWithChildren> = ({ children }) => {
  const classes = classNames(
    "fixed",
    "inset-0",
    "w-full",
    "h-full",
    "bg-white/50",
  );

  return <div className={classes}>{children}</div>;
};

const DialogContainer: React.FC<PropsWithChildren> = ({ children }) => {
  const classes = classNames("w-full", "max-w-[600px]", "mx-auto", "p-4");

  return <div className={classes}>{children}</div>;
};

interface ModalContentProps extends PropsWithChildren {
  className?: string;
}

export const ModalContent: React.FC<ModalContentProps> = ({
  children,
  className,
}) => {
  const classes = classNames("p-2", className);

  return <div className={classes}>{children}</div>;
};

export interface ModalProps extends PropsWithChildren {}

export const Modal: React.FC<ModalProps> = ({ children }) => {
  return (
    <DialogBackdrop>
      <DialogContainer>{children}</DialogContainer>
    </DialogBackdrop>
  );
};
