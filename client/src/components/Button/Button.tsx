import classNames from "classnames";

import { PropsWithAs, forwardRefWithAs } from "utils/withAs";

const variantPrimary = classNames(
  "bg-yellow-500",
  "hover:bg-yellow-600",

  "border-yellow-500",
  "hover:border-yellow-600",

  "text-slate-900",
);

const variantGhost = classNames(
  "bg-transparent",
  "hover:bg-gray-100",

  "border-transparent",
  "hover:border-gray-100",

  "text-gray-700",
);

const baseStyles = classNames(
  "inline-block",
  "py-2",
  "px-4",
  "border",
  "rounded",
  "font-medium",
);

type ButtonVariatns = "primary" | "ghost";

const variantDict: Record<ButtonVariatns, string> = {
  primary: variantPrimary,
  ghost: variantGhost,
};

type Sizes = "md" | "xl" | "xxxl";

const sizeMd = "";
const sizeXXXl = "text-6xl";
const sizeXl = "text-4xl";

const sizeDict: Record<Sizes, string> = {
  md: sizeMd,
  xl: sizeXl,
  xxxl: sizeXXXl,
};

interface ButtonComponentProps {
  variant?: ButtonVariatns;
  size?: Sizes;
}

export const ButtonComponent = (
  props: PropsWithAs<ButtonComponentProps, "button">,
  forwardRef: React.Ref<HTMLButtonElement>,
) => {
  const {
    as: Component = "button",
    variant = "primary",
    size = "md",
    className,
    children,
    ...rest
  } = props;

  const classes = classNames(
    baseStyles,
    variantDict[variant],
    sizeDict[size],
    className,
  );

  return (
    <Component className={classes} ref={forwardRef} {...rest}>
      {children}
    </Component>
  );
};

export type ButtonProps = PropsWithAs<ButtonComponentProps, "button" | "a">;
export const Button = forwardRefWithAs<ButtonComponentProps, "button" | "a">(
  ButtonComponent,
);
