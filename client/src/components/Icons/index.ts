import { createIcon } from "./createIcon";

import { ReactComponent as PowerOffSvg } from "./svg/power-off.svg";

export const PowerOffIcon = createIcon(PowerOffSvg);
