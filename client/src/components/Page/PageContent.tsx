import React from "react";

interface Props {}

export const PageContent: React.FC<React.PropsWithChildren<Props>> = ({
  children,
}) => {
  return <div className="py-4">{children}</div>;
};
