import React from "react";

interface Props {}

export const PageLayout: React.FC<React.PropsWithChildren<Props>> = ({
  children,
}) => {
  return <>{children}</>;
};
