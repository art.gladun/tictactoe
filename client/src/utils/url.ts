export interface PathParams {
  [key: string]: string;
}

interface Options {
  pathParams: PathParams;
}

export const composeUrl = (path: string, options: Options) => {
  return Object.entries(options.pathParams).reduce((acc, [key, value]) => {
    return acc.replace(`:${key}`, value);
  }, path);
};
