## How to

### 0. Monorepo (optional)

Install the dependencies on the root level first
It is optional becuase it is needed only when you want to commit a change

```
npm ci
```

### 1. Server

Install and deploy the server part to the AWS

```
npm ci
npx serverless deploy --stage dev --region eu-west-1
```

### 2. Client env variables
based on the created resources, create a `.env.local` file inside the `client` folder; example:

```
REACT_APP_WS_ENDPOINT=
REACT_APP_COGNITO_REGION=
REACT_APP_COGNITO_USER_POOL_ID=
REACT_APP_COGNITO_CLIENT_APP_ID=
```

### 3. Client

Install and run the client

```
npm ci
npm start
```

### 4. Open the browser and enjoy the game
