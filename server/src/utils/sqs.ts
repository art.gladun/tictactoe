import {
  SQSClient,
  ReceiveMessageCommand,
  SendMessageCommand,
  DeleteMessageCommand,
  ReceiveMessageCommandInput,
} from "@aws-sdk/client-sqs";

const sqsClient = new SQSClient({ region: process.env.AWS_REGION });

export const receiveMessage = async ({
  QueueUrl,
}: ReceiveMessageCommandInput) => {
  const command = new ReceiveMessageCommand({
    QueueUrl,
    MaxNumberOfMessages: 1,
    WaitTimeSeconds: 2,
  });

  const { Messages } = await sqsClient.send(command);

  if (Messages) {
    return Messages[0];
  }

  return null;
};

export const sendMessage = async <T>(message: T, { QueueUrl }: Options) => {
  const command = new SendMessageCommand({
    QueueUrl,
    MessageBody: JSON.stringify(message),
  });

  await sqsClient.send(command);
};

export const removeMessage = async (
  receiptHandle: string,
  { QueueUrl }: Options,
) => {
  const command = new DeleteMessageCommand({
    QueueUrl,
    ReceiptHandle: receiptHandle,
  });

  await sqsClient.send(command);
};
