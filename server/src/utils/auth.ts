import { APIGatewayEvent } from "aws-lambda";

export const getAuthUserId = (event: APIGatewayEvent) => {
  const authorizer = event.requestContext.authorizer!;
  return authorizer.userId || authorizer.lambda.userId;
};
