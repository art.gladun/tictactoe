import memoize from "lodash.memoize";

import { DynamoDBClient } from "@aws-sdk/client-dynamodb";
import { DynamoDBDocumentClient } from "@aws-sdk/lib-dynamodb";

const getDynamoDbClientFun = () => {
  const client = new DynamoDBClient({ region: process.env.AWS_REGION });
  return DynamoDBDocumentClient.from(client);
};

export const getDynamoDbClient = memoize(getDynamoDbClientFun);
