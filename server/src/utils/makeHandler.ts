import { Context, Callback, Handler } from "aws-lambda";

export function makeHandler<Event, Result>(handler: Handler<Event, Result>) {
  return async (event: Event, context: Context, callback: Callback) => {
    console.log(JSON.stringify(event));

    const result = await handler(event, context, callback);

    return result;
  };
}
