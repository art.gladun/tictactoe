import {
  ApiGatewayManagementApiClient,
  PostToConnectionCommand,
} from "@aws-sdk/client-apigatewaymanagementapi";

const apiWsClient = new ApiGatewayManagementApiClient({
  region: process.env.AWS_REGION,
  endpoint: process.env.WS_API_ENDPOINT,
});

export interface WSMessage<T = unknown> {
  data: T;
}

export const sendMessage = <T>(conectionIds: string[], data: WSMessage<T>) => {
  return Promise.all(
    conectionIds.map((connectionId: string) => {
      return apiWsClient.send(
        new PostToConnectionCommand({
          Data: JSON.stringify(data),
          ConnectionId: connectionId,
        }),
      );
    }),
  );
};
