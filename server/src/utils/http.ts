interface Options<T> {
  message?: string;
  statusCode?: number;
  data?: T;
  headers?: Record<string, string>;
}

interface Res {
  message?: string;
  statusCode?: number;
  body?: string;
  headers?: Record<string, string>;
}

export const buildResponse = <T>({
  message,
  statusCode,
  data,
  headers,
}: Options<T>): Res => {
  const res: Res = {};

  if (typeof message !== "undefined") {
    res.message = message;
  }

  if (typeof statusCode !== "undefined") {
    res.statusCode = statusCode;
  }

  if (typeof data !== "undefined") {
    res.body = JSON.stringify(data);
  }

  if (typeof headers !== "undefined") {
    res.headers = headers;
  }

  return res;
};
