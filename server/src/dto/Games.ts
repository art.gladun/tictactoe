import { PutCommand, DeleteCommand, QueryCommand } from "@aws-sdk/lib-dynamodb";

import { getDynamoDbClient } from "../utils/dynamodb";

const TableName = process.env.GAMES_TABLE!;

export type GameAction = "game-init" | "game-inprogress";

export interface Game {
  id: string;
  player1: string;
  player2?: string;
  connection1: string;
  connection2?: string;
  progress: string;
}

export const putGame = async (Item: Game) => {
  const dbClient = getDynamoDbClient();

  const command = new PutCommand({
    TableName,
    Item,
  });

  return dbClient.send(command);
};

export const deleteGame = async ({ id }: Pick<Game, "id">) => {
  const dbClient = getDynamoDbClient();

  const command = new DeleteCommand({
    TableName,
    Key: {
      id,
    },
  });

  return dbClient.send(command);
};

interface FilterProps {
  connectionId?: string;
}

export const getPlayer1Games = async ({
  player1,
  connectionId,
}: Pick<Game, "player1"> & FilterProps) => {
  const dbClient = getDynamoDbClient();

  const command = new QueryCommand({
    TableName,
    IndexName: "player1",
    FilterExpression: "connection1  = :connection1",
    KeyConditionExpression: "player1 = :player1",
    ExpressionAttributeValues: {
      ":player1": player1,
      ":connection1": connectionId,
    },
  });

  return dbClient.send(command);
};

export const getPlayer2Games = async ({
  player2,
  connectionId,
}: Pick<Game, "player2"> & FilterProps) => {
  const dbClient = getDynamoDbClient();

  const command = new QueryCommand({
    TableName,
    IndexName: "player2",
    FilterExpression: "connection2  = :connection2",
    KeyConditionExpression: "player2 = :player2",
    ExpressionAttributeValues: {
      ":player2": player2,
      ":connection2": connectionId,
    },
  });

  return dbClient.send(command);
};
