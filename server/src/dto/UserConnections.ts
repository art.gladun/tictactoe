import { PutCommand, DeleteCommand, QueryCommand } from "@aws-sdk/lib-dynamodb";

import { getDynamoDbClient } from "../utils/dynamodb";

const TableName = process.env.USER_CONNECTIONS_TABLE!;

export interface UserConnection {
  connectionId: string;
  userId: string;
}

export const getUserConnection = async ({
  connectionId,
}: Pick<UserConnection, "connectionId">) => {
  const dbClient = getDynamoDbClient();

  const command = new QueryCommand({
    TableName,
    KeyConditionExpression: "connectionId = :connectionId",
    ExpressionAttributeValues: {
      ":connectionId": connectionId,
    },
  });

  return dbClient.send(command);
};

export const putUserConnection = async (Item: UserConnection) => {
  const dbClient = getDynamoDbClient();

  const command = new PutCommand({
    TableName,
    Item,
  });

  return dbClient.send(command);
};

export const deleteUserConnection = async ({
  connectionId,
}: Pick<UserConnection, "connectionId">) => {
  const dbClient = getDynamoDbClient();

  const command = new DeleteCommand({
    TableName,
    Key: {
      connectionId,
    },
    ReturnValues: "ALL_OLD",
  });

  return dbClient.send(command);
};
