import { APIGatewayEvent } from "aws-lambda";

import { makeHandler } from "../../utils/makeHandler";
import { buildResponse } from "../../utils/http";

import { putUserConnection } from "../../dto/UserConnections";
import { getAuthUserId } from "../../utils/auth";

export const handler = makeHandler(async (event: APIGatewayEvent) => {
  const userId = getAuthUserId(event);
  const connectionId = event.requestContext.connectionId;

  if (typeof userId !== "string" || typeof connectionId !== "string") {
    return buildResponse({ statusCode: 400 });
  }

  await putUserConnection({ userId, connectionId });
  return buildResponse({
    statusCode: 200,
    headers: {
      "Sec-WebSocket-Protocol": event.headers["Sec-WebSocket-Protocol"]!,
    },
  });
});
