import { APIGatewayProxyWebsocketEventV2 } from "aws-lambda";

import { makeHandler } from "../../utils/makeHandler";
import { buildResponse } from "../../utils/http";
import { sendMessage as sendWsMessag } from "../../utils/ws";

import { putGame } from "../../dto/Games";

export const handler = makeHandler(
  async (event: APIGatewayProxyWebsocketEventV2) => {
    const senderConnectionId = event.requestContext.connectionId;

    if (typeof event.body !== "string") {
      return buildResponse({ statusCode: 500 });
    }

    let { data } = JSON.parse(event.body);
    let { connection1, connection2 } = data;

    await sendWsMessag(
      [connection1, connection2].filter(
        (connectionId: string) => connectionId !== senderConnectionId,
      ),
      { data },
    );
    await putGame(data);
    return buildResponse({ statusCode: 200 });
  },
);
