import { APIGatewayEvent } from "aws-lambda";

import { makeHandler } from "../../utils/makeHandler";
import { buildResponse } from "../../utils/http";

export const handler = makeHandler(async (event: APIGatewayEvent) => {
  return buildResponse({ statusCode: 200, message: "OK" });
});
