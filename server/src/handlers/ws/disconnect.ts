import { APIGatewayEvent } from "aws-lambda";

import { makeHandler } from "../../utils/makeHandler";
import { buildResponse } from "../../utils/http";
import { sendMessage as sendWsMessage } from "../../utils/ws";

import { deleteUserConnection } from "../../dto/UserConnections";
import { getPlayer1Games, getPlayer2Games } from "../../dto/Games";
import { getAuthUserId } from "../../utils/auth";

export const handler = makeHandler(async (event: APIGatewayEvent) => {
  const userId = getAuthUserId(event);
  const connectionId = event.requestContext.connectionId!;

  const [{ Items: games1 = [] }, { Items: games2 = [] }] = await Promise.all([
    getPlayer1Games({ player1: userId, connectionId }),
    getPlayer2Games({ player2: userId, connectionId }),
    deleteUserConnection({ connectionId }),
  ]);

  const [game] = [...games1, ...games2].filter(({ connection1, connection2 }) =>
    [connection1, connection2].includes(connectionId),
  );

  if (game) {
    await sendWsMessage([game.connection1, game.connection2], {
      data: {
        ...game,
        progress: "DISCONNECTED",
      },
    });
  }

  return buildResponse({ statusCode: 200, message: "OK" });
});
