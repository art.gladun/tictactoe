import { randomUUID } from "crypto";
import { APIGatewayEvent } from "aws-lambda";

import { makeHandler } from "../../utils/makeHandler";
import { buildResponse } from "../../utils/http";
import { sendMessage as sendWsMessage } from "../../utils/ws";
import { getAuthUserId } from "../../utils/auth";
import {
  receiveMessage as receiveSqsMessage,
  sendMessage as sendSqsMessage,
  removeMessage as removeSqsMessage,
} from "../../utils/sqs";

import { getUserConnection } from "../../dto/UserConnections";
import { Game, putGame } from "../../dto/Games";

const MATCHER_SQS_URL = process.env.MATCHER_SQS_URL!;

const findGameToJoin = async (userId: string): Promise<Game | null> => {
  console.log("find Game");

  let waitingGameMessage = await receiveSqsMessage({
    QueueUrl: MATCHER_SQS_URL,
  });

  if (!(waitingGameMessage && typeof waitingGameMessage.Body === "string")) {
    return null;
  }

  let waitingGame = JSON.parse(waitingGameMessage.Body);

  console.log(userId, waitingGame);

  if (waitingGame.player1 === userId) {
    return null;
  }

  await removeSqsMessage(waitingGameMessage.ReceiptHandle!, {
    QueueUrl: MATCHER_SQS_URL,
  });

  const { Items: connections = [] } = await getUserConnection({
    connectionId: waitingGame.connection1,
  });

  if (!connections.length) {
    return findGameToJoin(userId);
  }

  return waitingGame;
};

export const handler = makeHandler(async (event: APIGatewayEvent) => {
  const currentConnectionId = event.requestContext.connectionId!;
  const userId = getAuthUserId(event);

  if (typeof event.body !== "string") {
    return buildResponse({ statusCode: 500 });
  }

  const { data } = JSON.parse(event.body);

  const gameToJoin = await findGameToJoin(userId);

  if (gameToJoin) {
    const newGame: Game = {
      ...gameToJoin,
      player2: userId,
      connection2: currentConnectionId,
      progress: "PLAYER_1_TURN",
    };

    await putGame(newGame);
    await sendWsMessage([newGame.connection1, newGame.connection2!], {
      data: newGame,
    });
    return buildResponse({ statusCode: 200, data: newGame });
  }

  const newGame: Game = {
    ...data,
    id: randomUUID(),
    connection1: currentConnectionId,
    player1: userId,
    progress: "WAITING_FOR_OPPONENT",
  };

  await sendSqsMessage(newGame, { QueueUrl: MATCHER_SQS_URL });
  await sendWsMessage([newGame.connection1], {
    data: newGame,
  });

  return buildResponse({ statusCode: 200, data: newGame });
});
