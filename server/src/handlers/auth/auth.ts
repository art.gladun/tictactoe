import {
  APIGatewayRequestAuthorizerEventV2,
  APIGatewayRequestAuthorizerEvent,
} from "aws-lambda";
import { CognitoJwtVerifier } from "aws-jwt-verify";

const UserPoolId = process.env.USER_POOL_ID!;
const AppClientId = process.env.APP_CLIENT_ID!;

const getPolicyDocument = (effect: "Allow" | "Deny", resource: string) => {
  const policyDocument = {
    Version: "2012-10-17",
    Statement: [
      {
        Action: "execute-api:Invoke",
        Effect: effect,
        Resource: resource,
      },
    ],
  };

  return policyDocument;
};

const denyAllPolicy = () => {
  return {
    principalId: "*",
    policyDocument: getPolicyDocument("Deny", "*"),
  };
};

const allowPolicy = (methodArn: string, idToken: any) => {
  return {
    principalId: idToken.sub,
    policyDocument: getPolicyDocument("Allow", methodArn),
    context: {
      userId: idToken.sub,
    },
  };
};

export const handler = async (
  event: APIGatewayRequestAuthorizerEventV2 | APIGatewayRequestAuthorizerEvent,
) => {
  console.log(JSON.stringify(event, null, 2));

  try {
    const verifier = CognitoJwtVerifier.create({
      userPoolId: UserPoolId,
      tokenUse: "id",
      clientId: AppClientId,
    });

    const encodedToken =
      event.headers!.authorization || event.headers!["Sec-WebSocket-Protocol"];

    if (typeof encodedToken !== "string") {
      return denyAllPolicy();
    }

    const payload = await verifier.verify(encodedToken);
    console.log("Token is valid. Payload:", payload);

    const arn =
      (event as APIGatewayRequestAuthorizerEventV2).routeArn ||
      (event as APIGatewayRequestAuthorizerEvent).methodArn;

    return allowPolicy(arn, payload);
  } catch (error: any) {
    console.log(error.message);
    return denyAllPolicy();
  }
};
