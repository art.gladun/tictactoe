"use strict";

/**
 * It resolves prune config
 *
 * @param {object} serverless object
 * @returns prune config
 */
module.exports = async ({ resolveVariable }) => {
  const pruneNumber = await resolveVariable("env:pruneNumber");
  const pruneAutomatic = await resolveVariable("env:pruneAutomatic");

  return {
    automatic: pruneAutomatic === "true",
    number: Number(pruneNumber),
  };
};
